(asdf:defsystem :formulate
  :components
  ((:file "package")
   (:file "formulate" :depends-on ("package"))
   (:file "variables" :depends-on ("package" "formulate"))
   (:file "metaobjects" :depends-on ("package" "formulate"))))
