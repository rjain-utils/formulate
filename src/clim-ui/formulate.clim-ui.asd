(asdf:defsystem :formulate.clim-ui
  :components
  ((:file "package")
   (:file "application" :depends-on ("package"))
   (:file "variables" :depends-on ("package" "application"))
   (:file "objects" :depends-on ("package" "application"))
   (:file "classes" :depends-on ("package" "application")))
  :depends-on (:formulate :clim))
