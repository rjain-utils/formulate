(in-package :formulate.clim-ui)

(defvar *document-package-counter* 0)

(defun make-document-package ()
  (make-package (format nil "Document ~A" (incf *document-package-counter*))
                :use '(common-lisp formulate)))

(define-application-frame formulate ()
  ((document-package :initform (make-document-package) :initarg :document-package :accessor document-package)
   (monitored-values :initform (make-array 5 :fill-pointer 0 :adjustable t)
                     :accessor monitored-values))
  (:panes (interactor :interactor
                      :scroll-bars t
                      :min-width 400)
          (monitor :application
                   :scroll-bars t
                   :incremental-redisplay t
                   :display-function 'display-monitor
                   :display-time :command-loop))
  (:pointer-documentation t)
  (:layouts (default
                monitor
                interactor)))

(defmethod shared-initialize :after ((formulate formulate) slots &key)
  (setf (frame-pretty-name formulate)
        (format nil "Formulate: ~A" (package-name (document-package formulate)))))

(defmethod default-frame-top-level ((frame formulate) &key &allow-other-keys)
  (let ((*package* (document-package frame))
        (*debugger-hook* (if (find-package :clim-debugger)
                             (find-symbol "DEBUGGER" :clim-debugger)
                             *debugger-hook*)))
    (call-next-method)))

(define-formulate-command com-import-library ((name 'string))
  (use-package (string-upcase name)))

;;;;
;;;; DATA MONITOR
;;;;

(defun display-monitor (*application-frame* *standard-output*)
  (updating-output (t)
    (map nil (lambda (item) (display-monitored-value item))
         (monitored-values *application-frame*))))

(defmethod display-monitored-value :around (item)
  (updating-output (t :unique-id item)
    (call-next-method)))

(defun remove-from-monitor (value *application-frame*)
  (delete value (monitored-values *application-frame*)))

(defmacro display-formula-value (location)
  `(catch 'formula-value-fail
     (handler-bind 
         ((error #'(lambda (error) 
                     (display-formula-error error formulate::*formulating*)
                     (throw 'formula-value-fail nil))))
       (let* ((value ,location)
              (ptype (if (typep (class-of value) 'formulated-class)
                         'formulated-object
                         (presentation-type-of value))))
         (present value ptype)))))

(defmethod frame-standard-output ((frame formulate))
  (get-frame-pane frame 'interactor))

;;;;
;;;; FORMULA ERROR HANDLING
;;;;

(define-presentation-type formula-error ()
  :inherit-from t)

(defstruct formula-error
  error
  formulator)

(define-presentation-method present (error (type formula-error) stream (view textual-view)
                                           &key)
  (print-unreadable-object ((formula-error-error error) stream :type t)))

(defun display-formula-error (error formulator)
  (with-output-as-presentation (t (make-formula-error :error error :formulator formulator)
                                  'formula-error)
    (with-text-face (t :italic)
      (with-drawing-options (t :ink +red+)
2        (write-char #\!)
        (prin1 (class-name (class-of error)))
        (write-char #\!)))))

(define-formulate-command com-describe-error ((err 'formula-error :gesture :select))
  (present (formula-error-error err) t)
  (format t "~&while computing ~A" 
          (formulate::formulator-formula (formula-error-formulator err))))

;;;;
;;;; LAUNCHER ENTRY POINT
;;;;

(defun run ()
    (clim-sys:make-process 
     (lambda () 
       (clim:run-frame-top-level 
        (clim:make-application-frame "Formulate"
                                     :frame-class 'formulate.clim-ui:formulate)))))