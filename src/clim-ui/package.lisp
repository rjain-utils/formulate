(defpackage :formulate.clim-ui
  (:export #:formulate)
  (:use :clim-lisp :formulate :clim #.(first (list #+sbcl :sb-mop :mop))))
