(in-package :formulate.clim-ui)

(define-application-frame class-definer ()
  ((formulate-application :initarg :formulate-application :accessor formulate-application)
   (class-name :initarg :class-name :accessor name)
   (direct-superclasses :initarg :direct-superclasses 
                        :initform ()
                        :accessor direct-superclasses)
   (slots :initarg :direct-slots 
          :initform () 
          :accessor direct-slots)
   (options :initarg :options 
            :initform '((:metaclass formulated-class))
            :accessor options))
  (:pointer-documentation t)
  (:top-level (class-definer-top-level))
  (:geometry :width 500 :height 500)
  (:panes (class :application
                 :scroll-bars t
                 :display-function 'display-class-definition
                 :display-time :command-loop)
          (interactor :interactor 
                      :scroll-bars t))
  (:layouts (initial
             (:fill class)
             (horizontally (:x-spacing 10 :max-width 1000)
               (make-pane 'push-button :label "Define Class" :show-as-default t
                          :activate-callback
                          (lambda (gadget)
                            (declare (ignore gadget))
                            (eval-define-class *application-frame*)
                            (redisplay-frame-pane
                             (formulate-application *application-frame*)
                             'monitor)
                            (frame-exit *application-frame*)))
               (make-pane 'push-button :label "Cancel"
                          :activate-callback
                          (lambda (gadget)
                            (declare (ignore gadget))
                            (frame-exit *application-frame*)))))
            (interacting
             (2/5 class)
             (3/5 interactor))))

(define-formulate-command (com-define-formulated-class :name "Define Class")
    ((name 'symbol :prompt "Name"))
  (let ((package *package*)
        (formulate-application *application-frame*)
        (dialog-name (format nil "Define Class: ~A" name)))
    (clim-sys:make-process
     (lambda ()
       (let ((*package* package))
         (run-frame-top-level
          (apply #'make-application-frame dialog-name
                 :frame-class 'class-definer
                 :formulate-application formulate-application
                 :class-name name
                 (when (find-class name nil)
                   (let ((class (find-class name)))
                     (list :direct-superclasses (mapcar #'class-name (class-direct-superclasses class))
                           :direct-slots (mapcar #'make-slot-specification (class-direct-slots class))
                           :options (list (list :metaclass (class-name (class-of class)))))))))))
     :name dialog-name)))

 (defvar *setting-layout* nil)

(defmethod layout-frame ((frame class-definer) &optional width height)
  (if *setting-layout*
      nil #+nil ;; TODO need to figure out how to get relayout to happen without resizing frame
      (allocate-space (frame-top-level-sheet frame)
                      (graft-width (frame-top-level-sheet frame)) (graft-height (frame-top-level-sheet frame)))
      (if (or width height)
          (call-next-method)
          (let* ((sr-initial (compose-space (climi::find-pane-for-layout 'initial frame)))
                 (sr-interacting (compose-space (climi::find-pane-for-layout 'interacting frame)))
                 (combined-sr (space-requirement-combine #'max sr-initial sr-interacting)))
            (call-next-method frame
                              (space-requirement-width combined-sr)
                              (space-requirement-height combined-sr))))))

(defmethod (setf frame-current-layout) :around (name (frame class-definer))
  (let ((*setting-layout* t))
    (call-next-method)))

(defun accept-without-interactor (frame type)
  (with-input-context (type)
      (object)
      (loop (read-gesture :stream (get-frame-pane frame 'class)))
    (t object)))

(defvar *interaction-continuation* nil)

(defun do-interaction ()
  (let* ((interactor (get-frame-pane *application-frame* 'interactor))
         (*standard-input* interactor)
         (*standard-output* interactor)
         (*query-io* interactor))
    (window-clear (get-frame-pane *application-frame* 'interactor))
    (unwind-protect
         (funcall *interaction-continuation*)
      (setf *interaction-continuation* nil)
      (setf (frame-current-layout *application-frame*) 'initial))))

(defun get-command ()
  (let ((command
         (accept-without-interactor *application-frame*
                                    `(command :command-table ,(frame-command-table *application-frame*)))))
    (execute-frame-command *application-frame* command)))

(defun class-definer-top-level (*application-frame*)
  (redisplay-frame-panes *application-frame* :force-p t)
  ;; limit scope of changes to *INTERACTION-CONTINUATION* to this
  ;; specific invocation
  (let ((*interaction-continuation* nil))
    (loop
       (restart-case
           (progn
             (redisplay-frame-panes *application-frame*)
             (if (print *interaction-continuation*)
                 (do-interaction)
                 (get-command)))
         (abort ()
           :report "Return to application command loop"
           (clim-extensions:frame-display-pointer-documentation-string
            *application-frame*
            "Command aborted."))))))

(defmacro with-interaction (() &body body)
  `(progn
     (setf *interaction-continuation* (lambda () ,@body))
     (handler-case
         ;; the following may exit non-locally back to the top-level loop
         (setf (frame-current-layout *application-frame*) 'interacting)
       (climi::frame-layout-changed ()))
     (do-interaction)))

(defun eval-define-class (definer)
  (eval `(defclass ,(name definer) ,(direct-superclasses definer)
           ,(mapcar #'slot-specification-form (direct-slots definer))
           ,@(options definer))))

(define-presentation-type slot-specification ()
  :inherit-from 't)

(defstruct (slot-specification
             (:constructor %make-slot-specification
                           (name &key formula-p initform accessor options)))
  name
  formula-p
  initform
  accessor
  options)

(defmethod make-slot-specification ((slotd direct-slot-definition) &rest keys)
  (declare (ignore keys))
  (%make-slot-specification (slot-definition-name slotd)
                            :formula-p (subtypep (formulate::formulator-class slotd) 'formulate::formula-formulator-sink)
                            :initform (slot-definition-initform slotd)
                            :accessor (first (slot-definition-readers slotd))
                            :options nil))

(defmethod make-slot-specification ((name symbol) &rest keys)
  (apply #'%make-slot-specification name keys))

(defun slot-specification-form (spec)
  (list* (slot-specification-name spec)
         'formula-p (slot-specification-formula-p spec)
         :initform (slot-specification-initform spec)
         :accessor (slot-specification-accessor spec)
         (slot-specification-options spec)))

(define-presentation-method present ((spec slot-specification) (type slot-specification) *standard-output* view
                                     &key)
  (present (slot-specification-name spec))
  (with-text-face (t :italic)
    (prin1 " Formula?: "))
  (present (slot-specification-formula-p spec) 'boolean)
  (with-text-face (t :italic)
    (if (slot-specification-formula-p spec)
        (prin1 " Formula: ")
        (prin1 " Initial Value: ")))
  (present (slot-specification-initform spec) 'form)
  (with-text-face (t :italic)
    (prin1 " Accessor: "))
  (present (slot-specification-accessor spec) 'symbol)
  (with-text-face (t :italic)
    (prin1 " Options: "))
  (present (slot-specification-options spec) 'form))

(define-presentation-translator peer-accessor (slot-specification form class-definer)
    (object)
  (values `(my ,(slot-specification-name object)) 'expression t))

(define-presentation-translator peer-accessor (formulated-slot form class-definer)
    (object)
  (values `(my ,(second (third object))) 'expression t))

(define-presentation-method accept ((type slot-specification) *standard-output* view &key default)
  (let (name formula-p initform accessor options)
    (accepting-values ()
      (fresh-line)
      (setf name (apply #'accept 'symbol
                        :prompt "Name"
                        (when default
                          (list :default (slot-specification-name default)))))
      (terpri)
      (setf formula-p (accept 'boolean
                              :prompt "Formula?"
                              :default (if default
                                           (slot-specification-formula-p default)
                                           t)))
      (terpri)
      (setf initform (apply #'accept 'expression
                            :prompt (if formula-p "Formula" "Initial Value")
                            :query-identifier :initform
                            (when default
                              (list :default (slot-specification-initform default)))))
      (terpri)
      (setf accessor (apply #'accept 'symbol
                            :prompt "Accessor"
                            (when default
                              (list :default (slot-specification-accessor default)))))
      (terpri)
      (setf options (accept 'expression
                            :prompt "Options"
                            :default (if default
                                         (slot-specification-options default)
                                         nil))))
    (make-slot-specification name :formula-p formula-p :initform initform :accessor accessor :options options)))

(define-modify-macro nconcf (place &rest lists)
  nconc)

(defun add-superclass (gadget)
  (declare (ignore gadget))
  (with-interaction ()
    (let ((class (accept 'symbol)))
      (nconcf (direct-superclasses *application-frame*) 
              (list class)))))

(defun add-slot (gadget)
  (declare (ignore gadget))
  (with-interaction ()
    (let ((slot (accept 'slot-specification)))
      (nconcf (direct-slots *application-frame*) 
              (list slot)))))

(defun add-option (gadget)
  (declare (ignore gadget))
  (with-interaction ()
    (let ((option (accept 'form)))
      (nconcf (options *application-frame*) 
              (list option)))))

(define-presentation-type superclass ()
  :inherit-from 'class)

(define-class-definer-command (com-remove-superclass)
    ((superclass 'superclass :gesture :describe))
  (setf (direct-superclasses *application-frame*)
        (remove superclass (direct-superclasses *application-frame*))))

(define-class-definer-command (com-change-superclass)
    ((superclass 'superclass :gesture :select))
  (with-interaction ()
    (setf (direct-superclasses *application-frame*) 
          (substitute (direct-superclasses *application-frame*)
                      (accept 'symbol :default superclass :prompt "New Superclass")
                      superclass))))

(define-class-definer-command (com-remove-slot)
    ((slot 'slot-specification :gesture :describe))
  (setf (direct-slots *application-frame*) (remove slot (direct-slots *application-frame*))))

(define-class-definer-command (com-change-slot)
    ((slot 'slot-specification :gesture :select))
  (with-interaction ()
    (setf (direct-slots *application-frame*) 
          (substitute (accept 'slot-specification :default slot)
                      slot
                      (direct-slots *application-frame*)))))

(define-presentation-type class-option ()
  :inherit-from 'expression)

(define-presentation-method presentation-typep ((object cons) (type class-option))
  (and (symbolp (car object))
       (consp (cdr object))))

(define-class-definer-command (com-remove-option)
    ((option 'class-option :gesture :describe))
  (setf (options *application-frame*) (remove option (options *application-frame*))))

(define-class-definer-command (com-change-option)
    ((option 'class-option :gesture :select))
  (with-interaction ()
    (setf (options *application-frame*) 
          (substitute (accept 'class-option :default option)
                      option
                      (options *application-frame*)))))

(defun display-class-definition (*application-frame* *standard-output*)
  (with-look-and-feel-realization (*default-frame-manager* *application-frame*)
    (let ((interacting (eql (frame-current-layout *application-frame*) 'interacting)))
    (macrolet ((labelling ((&key label (newline-p t)) &body body)
                 `(surrounding-output-with-border (t)
                    (with-text-face (t :bold)
                      (princ ,label))
                    (write-char #\:)
                    ,(if newline-p
                        `(terpri)
                        `(write-char #\space))
                    ,@body)))
      (labelling (:label "Class Name" :newline-p nil)
        (princ (name *application-frame*)))
      (terpri)
      (labelling (:label "Superclasses")
        (filling-output (t)
          (dolist (super (direct-superclasses *application-frame*))
            (with-output-as-presentation (t super 'superclass)
              (princ super))
            (write-char #\Space)))
        (fresh-line)
        (with-output-as-gadget (t)
          (make-pane 'push-button :label "Add" :id 'add-superclass
                     :activate-callback #'add-superclass
                     :active-p (not interacting))))
      (terpri)
      (labelling (:label "Slots")
        (dolist (spec (direct-slots *application-frame*))
          (present spec 'slot-specification :single-box t)
          (terpri))
        (with-output-as-gadget (t)
          (make-pane 'push-button :label "Add" :id 'add-slot
                     :activate-callback #'add-slot
                     :active-p (not interacting))))
      (terpri)
      (labelling (:label "Options")
        (dolist (option (options *application-frame*))
          (present option 'class-option)
          (terpri))
        (with-output-as-gadget (t)
          (make-pane 'push-button :label "Add" :id 'add-option
                     :activate-callback #'add-option
                     :active-p (not interacting))))))))
