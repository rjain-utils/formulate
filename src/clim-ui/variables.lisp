(in-package :formulate.clim-ui)

(define-formulate-command (com-define-formulated-variable :name "Define Variable")
    ((name 'symbol :prompt "Name")
     (formula-p 'boolean :prompt "Formula?")
     (formula 'form :prompt (if formula-p "Formula" "Initial value"))
     (monitor-p 'boolean :default t :prompt "Show in monitor pane?")
     (declarations 'expression :default '((optimize (speed 1))) :prompt "Declarations")
     (documentation '(or string null) :default "" :prompt "Documentation"))
  (eval `(define-formulated-variable ,name ,formula
           :formula-p ,formula-p
           :declare ,declarations :documentation ,documentation))
  (when monitor-p
    (let ((*standard-output* (get-frame-pane *application-frame* 'monitor)))
      (vector-push-extend name (monitored-values *application-frame*)))))

(define-presentation-type formulated-variable ()
  :inherit-from 'symbol)

(defmethod display-monitored-value ((name symbol))
  (fresh-line)
  (with-output-as-presentation (t name (if (formulated-variable-p name) 'formulated-variable 'symbol) :single-box t)
    (formatting-table ()
      (formatting-row ()
        (formatting-cell ()
          (with-text-face (t :bold)
            (present name 'symbol))
          (write-string " = "))
        (formatting-cell ()
          (display-formula-value (eval name)))))
    (when (documentation name 'variable)
      (fresh-line)
      (with-text-face (t :italic)
        (with-drawing-options (t :ink +dark-green+)
          (filling-output (t)
            (write-string (documentation name 'variable)))))))
  (terpri)
  (terpri))

(define-formulate-command (com-remove-variable :name "Remove Variable From Monitor")
    ((name 'formulated-variable :prompt "Variable" :gesture :menu))
  (remove-from-monitor name *application-frame*))

(define-formulate-command (com-set-variable :name "Set Variable")
    ((name 'formulated-variable :gesture :select)
     (new-value 'expression))
  (eval `(setf ,name ',new-value)))

(define-formulate-command (com-describe-variable :name "Describe Variable")
    ((name 'formulated-variable :prompt "Name" :gesture :describe))
    (let ((formulator (let ((formulate::*get-formulator* t))
                        (eval name))))
      (format t "Variable ~A is computed by ~A~%using formula ~A"
              name formulator (formulate::formulator-formula formulator))))
