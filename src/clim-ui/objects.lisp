(in-package :formulate.clim-ui)

(define-formulate-command (com-create-instance :name "Create Instance")
    ((name 'symbol :prompt "Name")
     (class '(or class symbol) :prompt "Class"))
  (proclaim `(special ,name))
  (setf (symbol-value name) (make-instance class))
  (vector-push-extend name
                      (monitored-values *application-frame*)))

(define-presentation-type formulated-object ()
  :inherit-from t)

(define-presentation-type formulated-slot ()
  ;; 3 element list: (slot-value <object> <slot-name>)
  :inherit-from t)

(define-presentation-method presentation-typep (object (type formulated-object))
  (some (lambda (super) (typep super 'formulated-class))
        (class-precedence-list object)))

(defmethod display-slot-as-row (object slot stream view)
  (formatting-row (stream)
    (formatting-cell (stream)
      (with-text-face (stream :italic)
        (prin1 (slot-definition-name slot))
        (write-char #\: stream)))
    (formatting-cell (stream)
      (display-formula-value (slot-value object (slot-definition-name slot))))))

(define-presentation-method present (object (type formulated-object) stream view
                                            &key)
  (with-output-as-presentation (stream object 'formulated-object :single-box t)
    (with-output-as-presentation (stream (class-of object) 'class)
      (with-text-face (stream :bold)
        (with-drawing-options (t :ink +blue+)
          (prin1 (class-name (class-of object)) stream))))
    (fresh-line stream)
    (formatting-table (stream)
      (dolist (slot (class-slots (class-of object)))
        (with-output-as-presentation (stream
                                      `(slot-value ,object ',(slot-definition-name slot))
                                      'formulated-slot
                                       :single-box t)
          (display-slot-as-row object slot stream view))))))

(define-presentation-translator slot-accessor (formulated-slot form formulate)
    (object)
  (values object 'expression t))

(define-formulate-command (com-remove-object :name "Remove Object From Monitor")
    ((object 'formulated-object :prompt "Object" :gesture :menu))
  (remove-from-monitor object *application-frame*))

(define-formulate-command (com-set-slot-value :name "Set Slot Value")
    ((expression 'formulated-slot :prompt "Slot" :gesture :select)
     (new-value 'form :prompt "New value"))
  (destructuring-bind (s-v object (q slot)) expression
    (declare (ignore s-v q))
    (setf (slot-value object slot) (eval new-value))))

(define-formulate-command (com-describe-slot :name "Describe Slot")
    ((expression 'formulated-slot :prompt "Slot" :gesture :describe))
  (destructuring-bind (s-v object (q slot)) expression
    (declare (ignore s-v q))
    (let ((formulator (formulate::slot-formulator object slot)))
      (format t "Slot ~A of ~A is computed by ~A~%using formula ~A"
              slot object formulator (formulate::formulator-formula formulator)))))
