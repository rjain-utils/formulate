(in-package :formulate)

(deftype list-of (elt-type)
  'list)

;;;
;;; *** GENERAL DEFINITIONS ***
;;;

(defvar *get-formulator* nil)

(defvar *formulating* '()
  "The FORMULATOR, if any, that is being evaluated.")

(define-condition set-formulated-location (cell-error)
  ())

(defmethod formulator-value :around (formulator
                                     &optional unbound-condition cell-name)
  (when *formulating*
    (note-formula-dependency formulator *formulating*))
  (if (formulator-value-validp formulator)
      (call-next-method)
      (error unbound-condition :name cell-name)))

;;;
;;; *** SIMPLE FORMULATOR SOURCE ***
;;;

(defclass simple-formulator-source ()
  ((dependents :initform '() 
               :type (list-of formulator-sink) 
               :accessor formulator-dependents)
   (value))
  (:documentation "FORMULATOR-SOURCE implementation that unconditionally
  notifies all sinks that depend on it every time its value is changed."))

(defmethod initialize-instance :after ((formulator simple-formulator-source)
                                       &key ((formula formula)) ((formula-function formula-function)))
  (when formula-function
    (setf (slot-value formulator 'value) (funcall formula-function))))

(defmethod formulator-value-validp ((source simple-formulator-source))
  (slot-boundp source 'value))

(defmethod formulator-invalidate ((source simple-formulator-source))
  (slot-makunbound source 'value))

(defmethod formulator-value ((formulator simple-formulator-source) 
                             &optional cond cell)
  (slot-value formulator 'value))

(defmethod (setf formulator-value) (new-value (formulator simple-formulator-source))
  (let ((old-value (and (formulator-value-validp formulator)
                        (formulator-value formulator)))
        (result (setf (slot-value formulator 'value) new-value)))
    (dolist (dependent (formulator-dependents formulator))
      (formulator-value-changed dependent formulator new-value old-value))
    result))

;;;
;;; *** FORMULA FORMULATOR SINK ***
;;;

(defclass formula-formulator-sink ()
  ((formula :initarg formula
            :accessor formulator-formula)
   (formula-function :initarg formula-function
                     :initform (error "need to specify a formula-function")
                     :type function
                     :accessor formulator-formula-function))
  (:documentation "FORMULATOR-SINK implementation that recomputes the
  formula every time it is asked for a value."))

(defmethod formulator-value ((formulator formula-formulator-sink)
                             &optional cond cell)
  (funcall (formulator-formula-function formulator)))

(defmethod formulator-value-validp ((formulator formula-formulator-sink))
  (slot-boundp formulator 'formula))

;;;
;;; *** LAZY FORMULATOR ***
;;;

(defclass lazy-formulator-mixin ()
  ((source :initarg source
           :initform (make-instance 'simple-formulator-source)
           :accessor formulator-source
           :documentation "FORMULATOR-SOURCE that contains the cached
           value and propagates changes to sinks that refer to this
           formulator's parent cell."))
  (:documentation "Mixin that lazily recomputes and caches the formula's
  value."))

(defmethod formulator-dependents ((formulator lazy-formula-formulator-sink))
  (formulator-dependents (formulator-source formulator)))

(defmethod (setf formulator-dependents) (new-value (formulator lazy-formula-formulator-sink))
  (setf (formulator-dependents (formulator-source formulator)) new-value))

(defmethod formulator-value :around ((formulator lazy-formula-formulator-sink)
                                     &optional cond cell)
  ;; should this be an around method or should I mandate ordering of
  ;; mixins?
  (let ((source (formulator-source formulator)))
    (if (formulator-value-validp source)
        (let ((*formulating* nil))
          (formulator-value source cond cell))
        (let ((*formulating* formulator))
          ;; TODO: remove dependencies when dependencies change
          (setf (formulator-value source) (call-next-method))))))

(defmethod formulator-invalidate ((formulator lazy-formula-formulator-sink))
  (formulator-invalidate (formulator-source formulator)))

(defmethod note-formula-dependency (source sink)
  (pushnew sink (formulator-dependents source)))

(defmethod formulator-value-changed
    ((sink lazy-formula-formulator-sink) source new-value old-value)
  (formulator-invalidate sink))

(defclass lazy-formula-formulator-sink (lazy-formulator-mixin
                                        formula-formulator-sink)
  ()
  (:documentation "FORMULATOR-SINK implementation that lazily recomputes
  and caches the formula's value."))

;;;
;;; *** DYNAMIC FORMULA FORMULATOR ***
;;;

(defclass dynamic-formula-formulator-mixin (formula-formulator-sink)
  ())

(defmethod (setf formulator-value) (new-value (formulator dynamic-formulator))
  (setf (formulator-formula formulator) new-value
        (formulator-formula-function formulator) (compile nil `(lambda () ,new-value)))
  (formulator-invalidate formulator))

(defclass dynamic-lazy-formula-formulator (lazy-formula-formulator-sink dynamic-formula-formulator-mixin)
  ())
