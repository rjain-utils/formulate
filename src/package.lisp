(defpackage :formulate
  (:export #:formulator
           #:simple-formulator-source
           #:formula-formulator-sink
           #:lazy-formula-formulator-sink
           #:dynamic-lazy-formula-formulator
           #:formulated-class
           #:my
           #:*me*
           #:formula-p
           #:formulator-class
           #:formulator-options
           #:define-formulated-variable
           #:formulated-variable-p)
  (:use :cl #.(first '(#+sbcl :sb-mop :mop))))

(defpackage :formulate-user
  (:use :cl :formulate))

#+nil
(defpackage :formulate-meta
  (:export . #1=(#:*formulating*
                 #:formulator-value
                 #:formulator-formula
                 #:formulator-formula-function
                 #:formulator-value-validp
                 #:formulator-invalidate
                 #:simple-formulator-source
                 #:formula-formulator-sink
                 #:lazy-formula-formulator-sink
                 #:formulated-slot-definition
                 #:formulated-direct-slot-definition
                 #:formulated-effective-slot-definition
                 #:slot-formulator
                 #:slot-formulator-using-class))
  (:shadowing-import-from :formulate . #1#))
