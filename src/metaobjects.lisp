(in-package :formulate)

(defclass formulated-class (standard-class)
  ())

(defmethod validate-superclass ((class formulated-class) (super standard-class))
  t)

(defclass formulated-slot-definition (standard-slot-definition)
  ((formulator-class :initarg formulator-class
                     :accessor formulator-class)
   (formulator-options :initform '()
                       :initarg formulator-options
                       :accessor formulator-options)))

(defclass formulated-direct-slot-definition (formulated-slot-definition
                                             standard-direct-slot-definition)
  ())

(defmethod initialize-instance :after ((instance formulated-direct-slot-definition)
                                       &key ((formula-p formula-p)))
  "default formulator-class based on whether this is a formula or not"
  (unless (slot-boundp instance 'formulator-class)
    (setf (slot-value instance 'formulator-class)
          (if formula-p
              'lazy-formula-formulator-sink
              'simple-formulator-source))))

(defmethod slot-definition-initfunction ((slotd formulated-direct-slot-definition))
  (lambda () (apply 'make-instance (formulator-class slotd)
                    'formula (slot-definition-initform slotd)
                    'formula-function (call-next-method)
                    (formulator-options slotd))))

(defmethod direct-slot-definition-class ((class formulated-class) &key &allow-other-keys)
  ;; formula-p only indicates whether this is a formula sink as well as
  ;; a source.
  'formulated-direct-slot-definition)

(defclass formulated-effective-slot-definition (formulated-slot-definition
                                                standard-effective-slot-definition)
  ())

(defmethod effective-slot-definition-class ((class formulated-class) &key &allow-other-keys)
  ;; formula-p only indicates whether this is a formula sink as well as
  ;; a source.
  'formulated-effective-slot-definition)

(defmethod compute-effective-slot-definition ((class formulated-class) slot-name dslotds)
  (let ((eslotd (call-next-method))
        (most-specific-fdslotd
         (find-if 
          (lambda (slotd)
            (typep slotd 'formulated-direct-slot-definition)) 
          dslotds)))
    (setf (slot-value eslotd 'formulator-class) 
          (formulator-class most-specific-fdslotd))
    eslotd))

(defvar *me*)

(defmethod slot-value-using-class :around
    (class object (slotd formulated-effective-slot-definition))
  (if *get-formulator*
      (call-next-method)
      (let ((*me* object))
        (formulator-value (call-next-method) 'unbound-slot (slot-definition-name slotd)))))

(defmethod slot-formulator-using-class (class object (slotd formulated-effective-slot-definition))
  (let ((*get-formulator* t))
    (slot-value-using-class class object slotd)))

(defmethod (setf slot-value-using-class) :around
    (new-value
     class object (slotd formulated-effective-slot-definition))
  (if (slot-boundp-using-class class object slotd)
      (setf (formulator-value (slot-formulator-using-class class object slotd)) new-value)
      (call-next-method)))

(declaim (inline my))
(defun my (slot)
  (slot-value *me* slot))

(defun slot-formulator (object slot-name)
  (let ((*get-formulator* t))
    (slot-value object slot-name)))

(defmethod reinitialize-instance :after ((class formulated-class) &key)
  ;; TODO: u-i-f-r-c is not being called... find out why
  (eval `(defmethod update-instance-for-redefined-class :after
             ((instance ,(class-name class)) added discarded plist &rest initargs)
           ;; update formulae in slots
           ,@(mapcar (lambda (slotd)
                       `(unless (or (find ,(slot-definition-name slotd) added)
                                    ,(subtypep (formulator-class slotd) 'dynamic-formula-formulator-mixin))
                          (setf (formulator-formula (slot-formulator instance ,(slot-definition-name slotd)))
                                ,(slot-definition-initform slotd))
                          (setf (formulator-formula-function (slot-formulator instance ,(slot-definition-name slotd)))
                                ,(slot-definition-initfunction slotd))))
                     (class-slots class)))))
