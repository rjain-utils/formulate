(defprotocol formulator ())
(defprotocol formulator-source (formulator))
(defprotocol formulator-sink (formulator))
(defprotocol formula-formulator (formulator))

(defgeneric formulator-value (formulator &optional unbound-condition cell-name))
(defsignature formulator-value (formulator-source))

(defgeneric (setf formulator-value) (new-value formulator))
;; separate formulator-root that allows setf?
(defsignature (setf formulator-value) (t formulator-source))

(defgeneric formulator-value-validp (formulator))
(defsignature formulator-value-validp (formulator-source))

(defgeneric formulator-invalidate (formulator))
(defsignature formulator-invalidate (formulator-source))

(defgeneric formulator-dependents (formulator))
(defsignature formulator-dependents (formulator-source))

(defgeneric formulator-value-changed (sink source new-value old-value))
(defsignature formulator-value-changed (formulator-sink formulator-source t t))

(defgeneric formulator-formula (formulator)) ; change to formulator-formula-source?
(defsignature formulator-formula (formula-formulator)) ; change to formulator-formula-source?

(defgeneric formulator-formula-function (formulator)) ; change to formulator-function?
(defsignature formulator-formula-function (formula-formulator)) ; change to formulator-function?

(defgeneric (setf formulator-value) (new-formula formulator))
(defsignature (setf formulator-value) (function formula-formulator)) ; separate dynamic-formula-formulator subprotocol?

