(in-package :formulate)

(defmacro define-formulated-variable (name formula
                                      &key declare
                                           documentation
                                           (formula-p t)
                                           formulator-class
                                           formulator-options)
  `(progn
     (define-symbol-macro ,name (formulate-variable ',name))
     (setf (documentation ',name 'variable) ,documentation)
     (setf (symbol-value ',name)
           (make-instance ',(or formulator-class
                                (if formula-p
                                    'dynamic-formulator
                                    'simple-formulator-source))
                          'formula ',formula
                          'formula-function (lambda () (declare ,@declare) ,formula)
                          ,@formulator-options))
     (setf (get ',name 'formulated-variable-p) t)
     ',name))

(defun formulate-variable (name)
  (if *get-formulator*
      (symbol-value name)
      (formulator-value (symbol-value name) 'unbound-variable name)))

(defun (setf formulate-variable) (new-value name)
  (setf (formulator-value (symbol-value name)) new-value))

(defun formulated-variable-p (name)
  (get name 'formulated-variable-p nil))
