(in-package :formulate-user)

;; default is for variables to be formulas
(define-formulated-variable *x* 5 
  :formula-p nil)

(define-formulated-variable *y* (expt *x* 2))

(assert (= *x* 5))

(assert (= *y* 25))

(setf *x* 2)

(assert (= *y* 4))


(defgeneric area (shape))
(defgeneric perimeter (shape))

;; default is for slots to not be formulas --- is this inconsistency good or bad?
(defclass square ()
  ((side :initarg side :initform 0 :accessor square-side)
   (perimeter formula-p t :initform (* (my 'side) 4) :reader perimeter)
   (area formula-p t :initform (expt (my 'side) 2) :reader area))
  (:metaclass formulated-class))

(defclass polygon ()
  ((side :initarg side :initform 0 :accessor polygon-side)
   (num-sides :initarg num-sides :initform 1 :accessor polygon-num-sides)
   (perimeter formula-p t
              :initform (* (my 'side) (my 'num-sides))
              :reader perimeter)
   (area formula-p t
         :initform (* 1/4
                      (my 'num-sides) 
                      (expt (my 'side) 2)
                      (/ (tan (/ pi (my 'num-sides)))))
         :reader area))
  (:metaclass formulated-class))

(defclass circle ()
  ((radius :initform 0 :initarg radius :accessor circle-radius)
   (perimeter formula-p t :initform (* (my 'radius) 2 pi) :reader perimeter)
   (area formula-p t :initform (* (expt (my 'radius) 2) pi) :reader area))
  (:metaclass formulated-class))

(let ((square (make-instance 'square)))
  (dotimes (i 5)
    (setf (square-side square) i)
    (format t "~&A square with side ~A has perimeter ~A and area ~A~%"
            (square-side square) (perimeter square) (area square))))

(let ((circle (make-instance 'circle)))
  (dotimes (i 5)
    (setf (circle-radius circle) i)
    (format t "~&A circle with radius ~A has perimeter ~A and area ~A~%"
            (circle-radius circle) (perimeter circle) (area circle))))
